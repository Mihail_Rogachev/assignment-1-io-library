section .text

%define SPACE 0x20
%define TAB   0x9
%define NEW_LINE 0xA
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_LINE
    jmp print_char


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0
    xor rcx, rcx
    mov r10, 10
    .loop:
        xor rdx, rdx
        div r10
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        inc rcx
        test rax, rax
        jne .loop
    mov rdi, rsp    
    push rcx
    call print_string
    pop rcx
    inc rsp
    add rsp, rcx
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
        test rdi, rdi                 
        jns .positive                                            
        push rdi                     
        mov rdi, '-'                 
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
        .positive: 
            jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx,rcx                
    .loop:
        mov al, byte[rdi+rcx] 
        cmp byte[rsi+rcx], al 
        jne .false              
        cmp al, 0          
        je .true 
        inc rcx
        jmp .loop
    .false:
        mov rax, 0
        ret
    .true:
        mov rax, 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax                  
    xor rdi, rdi                  
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
     xor rcx,rcx                
    .loop:
        cmp rcx, rsi
        jge .end
        push rdi                
        push rsi
        push rcx
        call read_char          
        pop rcx                 
        pop rsi
        pop rdi
        cmp al, SPACE           
        je .space                
        cmp al, TAB             
        je .space
        cmp al, NEW_LINE        
        je .space
        mov byte[rdi+rcx], al    
        cmp al, 0                
        je .string_end
        inc rcx                 
        jmp .loop               
    .space:
        test rcx, rcx                 
        jz .loop                
    .string_end:                   
        mov rax, rdi            
        mov rdx, rcx            
        ret
    .end:
        xor rax, rax            
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        xor rax, rax
        xor rcx, rcx
        xor rdx, rdx
        .loop:
            mov dl, byte[rdi+rcx]
            sub dl, '0'
            cmp dl, 0
            js .end
            cmp dl, 10
            jns .end
            imul rax, 10
            add rax, rdx
            inc rcx
            jmp .loop
        .end:
            mov rdx, rcx
            ret
                                     

                                   

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret
    .positive:
        jmp parse_uint
 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jge .error
    push rax
    xor rcx, rcx
    .loop:
        mov al, [rdi+rcx]
        mov [rsi+rcx], al
        inc rcx
        cmp al, 0
        je .end
        jmp .loop
    .error:
        xor rax, rax
        ret
    .end:
        pop rax
        ret


